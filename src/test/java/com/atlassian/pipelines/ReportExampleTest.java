package com.atlassian.pipelines;

import org.junit.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ReportExampleTest {
    @Test
    public void testReturnTrueReturnsTrue() {
        assertThat(ReportExample.returnTrue()).isTrue();
    }

    @Test
    public void testReturnFalseReturnsFalse() {
        assertThat(ReportExample.returnFalse()).isFalse();
    }
    
    @Test
    public void testError() {
        throw new RuntimeException();
    }

    @Test
    public void testBlerg() {
        assertThat(true).isFalse();
    }

    @Test
    public void testError2() {
        throw new RuntimeException();
    }
    
    @Test
    public void testReturnTrueReturnsTrue1() {
        assertThat(ReportExample.returnTrue()).isTrue();
    }

    @Test
    public void testReturnFalseReturnsFalse1() {
        assertThat(ReportExample.returnFalse()).isFalse();
    }
    
    @Test
    public void testError1() {
        throw new RuntimeException();
    }

    @Test
    public void testBlerg1() {
        assertThat(true).isFalse();
    }

    @Test
    public void testError21() {
        throw new RuntimeException();
    }
    
    @Test
    public void testReturnTrueReturnsTrue2() {
        assertThat(ReportExample.returnTrue()).isTrue();
    }

    @Test
    public void testReturnFalseReturnsFalse2() {
        assertThat(ReportExample.returnFalse()).isFalse();
    }
    
    @Test
    public void testError24() {
        throw new RuntimeException();
    }

    @Test
    public void testBlerg23() {
        assertThat(true).isFalse();
    }

    @Test
    public void testError223() {
        throw new RuntimeException();
    }
    
        @Test
    public void testReturnTrueReturnsTrue3() {
        assertThat(ReportExample.returnTrue()).isTrue();
    }

    @Test
    public void testReturnFalseReturnsFalse3() {
        assertThat(ReportExample.returnFalse()).isFalse();
    }
    
    @Test
    public void testError3() {
        throw new RuntimeException();
    }

    @Test
    public void testBlerg3() {
        assertThat(true).isFalse();
    }

    @Test
    public void testError23() {
        throw new RuntimeException();
    }
}
